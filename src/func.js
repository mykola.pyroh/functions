const getSum = (str1, str2) => {
  const isString = str => typeof str === 'string'; 
  if (!isString(str1) || !isString(str2) || isNaN(str1) || isNaN(str2)) {
    return false;
  }
  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const counter = (prev, curr) => curr.author === authorName ? prev + 1 : prev;
  const countOfPosts = listOfPosts
    .reduce(counter, 0);
  const coutOfComments = listOfPosts
    .reduce((prev, curr) => curr.comments ? [...prev, ...curr.comments] : prev, [])
    .reduce(counter, 0);
  
  return `Post:${countOfPosts},comments:${coutOfComments}`;
};

const tickets=(people)=> {
  const ticket = 25;
  let availableChange = 0;
  for (let person of people) {
    const change = person - ticket;
    if (change <= availableChange) {
      availableChange = availableChange - change + person;
    }
    else {
      return 'NO';
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
